package com.example.samuellucas.ondedoarsangue.ui.ondedoar

import android.app.Activity
import android.os.Bundle
import com.example.samuellucas.ondedoarsangue.R
import com.example.samuellucas.ondedoarsangue.data.mock.BancoDeSangue
import com.example.samuellucas.ondedoarsangue.data.mock.BancoDeSangueMock
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.content.Intent
import android.view.MenuItem
import com.example.samuellucas.ondedoarsangue.ui.main.MainUser


class OndeDoar : Activity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onde_doar)

        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeButtonEnabled(true)


        // Obtain the MapFragment and get notified when the map is ready to be used.
        val mapFragment = fragmentManager
                .findFragmentById(R.id.map) as MapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        populateMap(googleMap)
    }

    private fun getMarkerColor(estoque_status: String): BitmapDescriptor {
        return when(estoque_status) {
            "alta" -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)
            "media" -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)
            "baixa" -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)
            else -> BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)
        }
    }

    private fun getMarkerTitle(): String {
        return "Default Marker Title"
    }

    private fun populateMap(googleMap: GoogleMap){
        mMap = googleMap

        for (bancoDeSangue: BancoDeSangue in getBancosDeSangue()){
            mMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(bancoDeSangue.lat, bancoDeSangue.lng))
                        .icon(getMarkerColor(bancoDeSangue.estoque_x))
                        .title(bancoDeSangue.nome_banco)
                        .snippet(bancoDeSangue.estoque_x)
            )

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(bancoDeSangue.lat, bancoDeSangue.lng), 13.0F));
        }

    }

    private fun getBancosDeSangue(): List<BancoDeSangue> {
        val bancosDeSangue: MutableList<BancoDeSangue> = BancoDeSangueMock.getBancosDeSangueMocked()

        return bancosDeSangue
    }


    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, MainUser::class.java))
        finishAffinity()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { //Botão adicional na ToolBar
        when (item.itemId) {
            android.R.id.home  //ID do seu botão (gerado automaticamente pelo android, usando como está, deve funcionar
            -> {
                startActivity(Intent(this, MainUser::class.java))  //O efeito ao ser pressionado do botão (no caso abre a activity)
                finishAffinity()  //Método para matar a activity e não deixa-lá indexada na pilhagem
            }
            else -> {
            }
        }
        return true
    }

}
