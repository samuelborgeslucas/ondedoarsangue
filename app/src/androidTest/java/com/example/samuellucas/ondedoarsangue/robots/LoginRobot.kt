package com.example.samuellucas.ondedoarsangue.robots

import com.example.samuellucas.ondedoarsangue.R
import com.example.samuellucas.ondedoarsangue.base.BaseTestRobot
import kotlinx.android.synthetic.main.activity_sign_in.view.*

fun login(func: LoginRobot.() -> Unit) = LoginRobot().apply { func() }

class LoginRobot : BaseTestRobot() {

    fun setEmail(email: String) = fillEditText(R.id.email, email)

    fun setPassword(pass: String) = fillEditText(R.id.password, pass)

    fun clickLoginDonator() = clickButton(R.id.login_doador)

    fun clickLoginBanco() = clickButton(R.id.login_banco)

    fun clickCadastrar() = clickButton(R.id.cadastrar)

    fun matchErrorText(err: String) = matchText(textView(0), err)

    fun matchText(txt: String) = matchText(textView(0), txt)

}