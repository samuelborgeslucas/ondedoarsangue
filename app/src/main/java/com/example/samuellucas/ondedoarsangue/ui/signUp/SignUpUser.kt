package com.example.samuellucas.ondedoarsangue.ui.signUp

import android.os.Bundle
import android.app.Activity
import com.example.samuellucas.ondedoarsangue.R

import kotlinx.android.synthetic.main.activity_sign_up_user.*

class SignUpUser : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_user)
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
