package com.example.samuellucas.ondedoarsangue.base

import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.anything

open class BaseTestRobot {

    fun fillEditText(resId: Int, text: String): ViewInteraction =
        onView(withId(resId)).perform(ViewActions.replaceText(text), ViewActions.closeSoftKeyboard())

    fun clickButton(resId: Int): ViewInteraction =
        onView(withId(resId)).perform(ViewActions.click())

    fun textView(resId: Int): ViewInteraction =
        onView(withId(resId))

    fun matchText(viewInteraction: ViewInteraction, text: String): ViewInteraction =
        viewInteraction.check(ViewAssertions.matches(ViewMatchers.withText(text)))

    fun matchText(resId: Int, text: String): ViewInteraction =
        matchText(textView(resId), text)

    fun clickListItem(listRes: Int, position: Int): ViewInteraction =
        onData(anything())
                .inAdapterView(allOf(withId(listRes)))
                .atPosition(position).perform(ViewActions.click())
}


// Like Builder pattern each method should return itself.
// So all actions can be chained step by step.