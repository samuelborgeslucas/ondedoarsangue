package com.example.samuellucas.ondedoarsangue.ui.signUp

import android.os.Bundle
import android.app.Activity
import com.example.samuellucas.ondedoarsangue.R

import kotlinx.android.synthetic.main.activity_sign_up_blood_bank.*

class SignUpBloodBank : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_blood_bank)
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
