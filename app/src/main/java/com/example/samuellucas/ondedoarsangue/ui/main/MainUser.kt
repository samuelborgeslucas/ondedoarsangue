package com.example.samuellucas.ondedoarsangue.ui.main

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import com.example.samuellucas.ondedoarsangue.R
import com.example.samuellucas.ondedoarsangue.ui.ondedoar.OndeDoar

import kotlinx.android.synthetic.main.activity_main_user.*

class MainUser : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_user)

        card_onde_doar.setOnClickListener { goToOndeDoar() }
    }

    private fun goToOndeDoar(){
        val ondeDoar = Intent(this, OndeDoar::class.java)
        startActivity(ondeDoar)
    }

}
