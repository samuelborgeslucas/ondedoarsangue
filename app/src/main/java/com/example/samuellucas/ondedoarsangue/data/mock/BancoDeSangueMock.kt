package com.example.samuellucas.ondedoarsangue.data.mock

open class BancoDeSangueMock {
    companion object {
        val santaCasa = BancoDeSangue(-30.0303677, -51.2237683, "Santa Casa", "media")
        val hospDeClinicas = BancoDeSangue(-30.0387223, -51.2073265, "Hosp de Clínicas", "alta")
        val hemocentroRs = BancoDeSangue(-30.0627793, -51.1806828, "Hemocentro", "baixa")
        val labMarquesPereira = BancoDeSangue(-30.0313296, -51.2269425, "Lab Marques Pereira", "media")
        val hospSaoLucasPucrs = BancoDeSangue(-30.0551241, -51.1772088, "Hosp São Lucas PUCRS", "alta")
        val hospMaeDeDeus = BancoDeSangue(-30.0586531, -51.2313639, "Hosp Mãe de Deus", "baixa")
        val hospConceicao = BancoDeSangue(-30.0159434, -51.1606252, "Hosp Conceição", "media")
        val hospDivinaProvidencia = BancoDeSangue(-30.0848175, -51.1901265, "Hosp Divina Providencia", "media")
        val hospMoinhosDeVento = BancoDeSangue(-30.0256917, -51.2105876, "Hosp Moinhos de Vento", "media")

        public fun getBancosDeSangueMocked() : MutableList<BancoDeSangue> {
            var bancosDeSangue: MutableList<BancoDeSangue> = mutableListOf()

            bancosDeSangue.add(santaCasa)
            bancosDeSangue.add(hospDeClinicas)
            bancosDeSangue.add(hemocentroRs)
            bancosDeSangue.add(labMarquesPereira)
            bancosDeSangue.add(hospSaoLucasPucrs)
            bancosDeSangue.add(hospMaeDeDeus)
            bancosDeSangue.add(hospConceicao)
            bancosDeSangue.add(hospDivinaProvidencia)
            bancosDeSangue.add(hospMoinhosDeVento)

            return bancosDeSangue
        }
    }

//    LatLng santaCasa = new LatLng(-30.0303677, -51.2237683);
//    LatLng hospDeClinicas = new LatLng(-30.0387223, -51.2073265);
//    LatLng hemocentroRs = new LatLng(-30.0627793, -51.1806828);
//    LatLng labMarquesPereira = new LatLng(-30.0313296, -51.2269425);
//    LatLng hospSaoLucasPucrs = new LatLng(-30.0551241, -51.1772088);
//    LatLng hospMaeDeDeus = new LatLng(-30.0586531, -51.2313639);
//    LatLng hospConceicao = new LatLng(-30.0159434, -51.1606252);
//    LatLng hospDivinaProvidencia = new LatLng(-30.0848175, -51.1901265);
//    LatLng hospMoinhosDeVento = new LatLng(-30.0256917, -51.2105876);
//
//    mMap.addMarker(new MarkerOptions().position(santaCasa).title("Santa Casa"));
//    mMap.addMarker(new MarkerOptions().position(hospDeClinicas).title("Hosp de Clínicas"));
//    mMap.addMarker(new MarkerOptions().position(hemocentroRs).title("Hemocentro"));
//    mMap.addMarker(new MarkerOptions().position(labMarquesPereira).title("Lab Marques Pereira"));
//    mMap.addMarker(new MarkerOptions().position(hospSaoLucasPucrs).title("Hosp São Lucas PUCRS"));
//    mMap.addMarker(new MarkerOptions().position(hospMaeDeDeus).title("Hosp Mãe de Deus"));
//    mMap.addMarker(new MarkerOptions().position(hospConceicao).title("Hosp Conceição"));
//    mMap.addMarker(new MarkerOptions().position(hospDivinaProvidencia).title("Hosp Divina Providencia"));
//    mMap.addMarker(new MarkerOptions().position(hospMoinhosDeVento).title("Hosp Moinhos de Vento"));



}