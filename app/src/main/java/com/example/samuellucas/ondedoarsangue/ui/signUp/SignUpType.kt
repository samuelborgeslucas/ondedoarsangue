package com.example.samuellucas.ondedoarsangue.ui.signUp

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.samuellucas.ondedoarsangue.R
import kotlinx.android.synthetic.main.activity_sign_up_type.*

class SignUpType : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_type)

        cadastrar_doador.setOnClickListener { cadastrarDoador() }
        cadastrar_banco_de_sangue.setOnClickListener { cadastrarBancoDeSangue() }
    }

    private fun cadastrarDoador(){
        val signUpUser = Intent(this, SignUpUser::class.java)
        startActivity(signUpUser)
    }

    private fun cadastrarBancoDeSangue(){
        val signUpBloodBank = Intent(this, SignUpBloodBank::class.java)
        startActivity(signUpBloodBank)
    }
}
