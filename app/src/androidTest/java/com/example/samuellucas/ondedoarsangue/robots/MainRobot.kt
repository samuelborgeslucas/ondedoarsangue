package com.example.samuellucas.ondedoarsangue.robots

import com.example.samuellucas.ondedoarsangue.base.BaseTestRobot

fun main(func: MainRobot.() -> Unit) = MainRobot().apply { func() }

class MainRobot : BaseTestRobot() {

}