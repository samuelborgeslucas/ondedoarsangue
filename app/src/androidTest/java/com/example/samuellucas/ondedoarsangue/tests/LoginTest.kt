package com.example.samuellucas.ondedoarsangue.tests

import android.support.test.rule.ActivityTestRule
import com.example.samuellucas.ondedoarsangue.R
import com.example.samuellucas.ondedoarsangue.ui.signIn.SignInActivity
import com.example.samuellucas.ondedoarsangue.robots.login
import org.junit.Rule
import org.junit.Test

class LoginTest {

    @get:Rule
    val mActivityTestRule: ActivityTestRule<SignInActivity> = ActivityTestRule(SignInActivity::class.java)

    @Test
    fun loginDoadorMissingEmailPassword(){
        login {
            clickLoginDonator()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginBancoMissingEmailPassword(){
        login {
            clickLoginBanco()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginDoadorMissingPassword(){
        login {
            setEmail("samuel@gmail.com")
            clickLoginDonator()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginBancoMissingPassword(){
        login {
            setEmail("samuelbanco@gmail.com")
            clickLoginBanco()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginDoadorMissingEmail(){
        login {
            setPassword("12345")
            clickLoginDonator()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginBancoMissingEmail(){
        login {
            setPassword("12345")
            clickLoginBanco()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginDoadorWithWrongCredentials(){
        login {
            setEmail("wrongemail@gmail.com")
            setPassword("wrongpass")
            clickLoginDonator()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginBancoWithWrongCredentials(){
        login {
            setEmail("wrongemail@gmail.com")
            setPassword("wrongpass")
            clickLoginBanco()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginDoadorWithBancoData(){
        login {
            setEmail("samuelbanco@gmail.com")
            setPassword("12345")
            clickLoginDonator()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginBancoWithDoadorData(){
        login {
            setEmail("samuel@gmail.com")
            setPassword("12345")
            clickLoginBanco()
            matchErrorText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginDoadorSuccessful(){
        login {
            setEmail("samuel@gmail.com")
            setPassword("12345")
            clickLoginDonator()
            matchText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun loginBancoSuccessful(){
        login {
            setEmail("samuel@gmail.com")
            setPassword("12345")
            clickLoginBanco()
            matchText(string(R.string.abc_action_bar_home_description))
        }
    }

    @Test
    fun startSignUp(){
        login {
            clickCadastrar()
            matchText(string(R.string.abc_action_bar_home_description))
        }
    }


    private fun string(res: Int): String = mActivityTestRule.activity.getString(res)


}